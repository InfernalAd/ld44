﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamProjectile : Projectile {
    public override void OnCollisionEnter() {
        GetComponent<Collider2D>().enabled = false;
    }

    public void Deactivate() {
        GetComponent<Animator>().SetBool("Active", false);
    }

    protected override void Process(float deltaTime) {
    }
}
