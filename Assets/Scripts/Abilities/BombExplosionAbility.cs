﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplosionAbility : Ability {
	private Action _action;

	public BombExplosionAbility(float duration, float cooldown,Action activateAction) : base("explosion", duration, cooldown) {
		_action = activateAction;
	}

	protected override void ChangeState(State state) {
		base.ChangeState(state);
		switch (state) {
				case State.Active:
					_action();
					break;
		}
	}
}
