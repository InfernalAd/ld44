﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTimeAbility : Ability {
  public BulletTimeAbility(float duration, float cooldown) : base("bullet_time", duration, cooldown) {
  }

  protected override void ChangeState(State state) {
    base.ChangeState(state);
    switch (state) {
        case State.Active:
          LevelController.Instance.SetBulletTimeActive(true);
          break;
      case State.Cooldown:
        LevelController.Instance.SetBulletTimeActive(false);
        break;
    }
  }
}
