﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Timeline;

public class Ability {
  public enum State {
    Disabled =0,
    Cooldown = 1,
    Active = 2,
    WaitingForActication = 3
  }

  public State CurrentState { get { return _state; } }
  public string Type { get; private set; }
  
  private float _duration;
  private float _cooldown;
  
  
  private State _state;

  private float _timer = 0f;

  public Ability(string type,float duration,float cooldown) {
    _duration = duration;
    _cooldown = cooldown;
    Type = type;
  }

  public void Process(float deltaTime) {
    if (_state == State.Disabled) return;
    if (_state == State.Active) {
      _timer -= deltaTime;
      if (_timer < 0) {
        ChangeState(State.Cooldown);
        _timer = _cooldown;
      }
    }
    if (_state == State.Cooldown) {
      _timer -= deltaTime;
      if (_timer < 0) {
        ChangeState(State.WaitingForActication);
      }
    }
  }

  protected virtual void ChangeState(State state) {
    _state = state;
  }

  public void Enable() {
    if (_state == State.Disabled) {
      ChangeState(State.WaitingForActication);
    }
  }

  public void Activate() {
    if (_state == State.WaitingForActication) {
      ChangeState(State.Active);
      _timer = _duration;
    }
  }
}
