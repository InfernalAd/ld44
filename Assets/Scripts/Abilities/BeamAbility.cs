﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamAbility : Ability {
	private Action<bool> _shieldAction;

	public BeamAbility(float duration, float cooldown,Action<bool> shieldActivateAction) : base("beam", duration, cooldown) {
		_shieldAction = shieldActivateAction;
	}

	protected override void ChangeState(State state) {
		base.ChangeState(state);
		switch (state) {
				case State.Active:
					_shieldAction(true);
					break;
			case State.Cooldown:
				_shieldAction(false);
				break;
		}
	}
}
