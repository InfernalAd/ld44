﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldAbility : Ability {
	private Action<bool> _shieldAction;

	public ShieldAbility(float duration, float cooldown,Action<bool> shieldActivateAction) : base("shield", duration, cooldown) {
		_shieldAction = shieldActivateAction;
	}

	protected override void ChangeState(State state) {
		base.ChangeState(state);
		switch (state) {
				case State.Active:
					_shieldAction(true);
					break;
			case State.Cooldown:
				_shieldAction(false);
				break;
		}
	}
}
