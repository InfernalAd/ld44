﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKnight : Enemy {

    public Vector2 _speed;
    public float _dispersionX;
    private float _startX;
    private bool _movingLeft;

    private void Start() {
        _startX = transform.position.x;
    }

    protected override void Process(float deltaTime) {
        _weapons.ForEach(x => x.Shoot());

        if (_movingLeft && (transform.position.x <= _startX - _dispersionX)) {
            _movingLeft = !_movingLeft;
        }
        if (!_movingLeft && (transform.position.x >= _startX + _dispersionX)) {
            _movingLeft = !_movingLeft;
        }
        var speed = _movingLeft ? new Vector2(-_speed.x, _speed.y) : _speed;
        transform.position += new Vector3(speed.x, speed.y) * deltaTime;
        if (!LevelController.Instance.IsInBounds(transform.position)) {
            Destroy();
        }
    }
}
