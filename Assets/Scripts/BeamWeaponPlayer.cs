﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamWeaponPlayer : Weapon {
    public bool _shootingUp;
    private float _shootingTimer = 1f;

    private List<BeamProjectile> _projectiles = new List<BeamProjectile>();

    protected override void Process(float deltaTime) {
        if (_shootingTimer > 0) {
            _shootingTimer -= deltaTime;
        }
        else {
            base.Process(deltaTime);
        }
    }

    public override void Shoot() {
        var positionDiff = Vector3.zero;
        for (int i = 0; i != 50; i++) {

            var proj = Instantiate(_projectilePrefab, transform);
            proj.InitAsBeam();
            proj.transform.position = transform.position + positionDiff;
            positionDiff += new Vector3(0, _shootingUp ? 0.06f : -0.06f);
            _projectiles.Add((BeamProjectile) proj);
        }
        _shootingTimer = 1.6f;
    }

    public void Deactivate() {
        _projectiles.ForEach(x=>x.Deactivate());
        _projectiles.Clear();
    }

    public override bool IsShooting() {
        return _shootingTimer > 0;
    }
}
