﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : MonoBehaviour {
 
    [Serializable]
    private class AbilityInfo {
        public int Duration;
        public int Cooldown;
        public int Price;
    }


    [SerializeField] private float _speedForward;
    [SerializeField] private float _speedBack;
    [SerializeField] private float _speedSide;
    [SerializeField] private AbilityInfo _shieldAbility;
    [SerializeField] private AbilityInfo _bombAbility;
    [SerializeField] private AbilityInfo _beamAbility;
    [SerializeField] private AbilityInfo _bulletTimeAbility;

    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private LevelController.Bounds _bounds;

    [SerializeField] private Weapon _weapon;
    [SerializeField] private BeamWeaponPlayer _beamWeapon;
    [SerializeField] private GameObject _shield;
    

    private List<Ability> _abilities = new List<Ability>();
    private bool _isShielded;
    [SerializeField]
    private int _maxHp;
    
    private int _hp;

    private bool _beamWeaponActive;
    public int MaxHp { get { return _maxHp;} }
    public int Hp { get { return _hp;} }

    void Awake () {
        LevelController.Instance.OnProcess += Process;
        LevelController.Instance.OnProcessUnscaled += ProcessUnscaled;
        _abilities.Add(new ShieldAbility(_shieldAbility.Duration,_shieldAbility.Cooldown,
            (active) => {
                _shield.SetActive(active);
                _isShielded = active;
            }));
        _abilities.Add(new BeamAbility(_beamAbility.Duration,_beamAbility.Cooldown,
            (active) => {
                _beamWeaponActive = active;
                if (active) {
                    _beamWeapon.Shoot();
                }
                else {
                    _beamWeapon.Deactivate();
                }
            }));
        _abilities.Add(new BombExplosionAbility(0,_bombAbility.Cooldown,
            () => {
                LevelController.Instance.CreateBombExplosion(transform.position);
            }));
        _abilities.Add(new BulletTimeAbility(_bulletTimeAbility.Duration,_bulletTimeAbility.Cooldown));
        RefillHp();
    }

    private void ProcessUnscaled(float dt) {
        _abilities.ForEach(x=>x.Process(dt));
    }

    private void OnDestroy() {
        LevelController.Instance.OnProcess -= Process;
    }

    public void ActivateAbility(string abilityType) {
        var ability = _abilities.Find(x => x.Type.Equals(abilityType));
        if (ability != null) {
            ability.Activate();
        }
    }
    

    void Process (float deltaTime) {
        var deltaVector = Vector2.zero;
        if (Input.GetKey(KeyCode.LeftArrow)) {
            deltaVector += Vector2.left * _speedSide;
        }
        if (Input.GetKey(KeyCode.RightArrow)) {
            deltaVector += Vector2.right * _speedSide;
        }
        if (Input.GetKey(KeyCode.UpArrow)) {
            deltaVector += Vector2.up * _speedForward;
        }
        if (Input.GetKey(KeyCode.DownArrow)) {
            deltaVector += Vector2.down * _speedBack;
        }
        if (Input.GetKey(KeyCode.C)) {
            ActivateAbility("shield");
        }
        if (Input.GetKey(KeyCode.Z)) {
            ActivateAbility("bullet_time");
        }
        if (Input.GetKey(KeyCode.X)) {
            ActivateAbility("explosion");
        }
        if (Input.GetKey(KeyCode.V)) {
            ActivateAbility("beam");
        }
        if (!_beamWeaponActive)
            _weapon.Shoot();
        var newPos = transform.position + new Vector3(deltaVector.x, deltaVector.y)*deltaTime;
        var clampX = Mathf.Clamp(newPos.x, _bounds.xMin, _bounds.xMax);
        var clampY = Mathf.Clamp(newPos.y, _bounds.yMin, _bounds.yMax);
        transform.position = new Vector3(clampX, clampY, 0);
    }

    void OnCollisionEnter2D(Collision2D coll) {
        var projectile = coll.gameObject.GetComponent<Projectile>();
        var rewardItem = coll.gameObject.GetComponent<RewardItem>();
        var enemy = coll.gameObject.GetComponent<Enemy>();
        if (projectile != null) { 
            if (_isShielded) return;
            projectile.OnCollisionEnter();
            _hp -= projectile.Damage;
            if (_hp <= 0) {
                LevelController.Instance.PlayExplosionFX(transform.position);
                GameObject.Destroy(gameObject);
            }
        }
        if (enemy != null) { 
            if (_isShielded) return;
            enemy.Destroy();
            _hp -= 2;
            if (_hp <= 0) {
                LevelController.Instance.PlayExplosionFX(transform.position);
                GameObject.Destroy(gameObject);
            }
        }

        if (rewardItem != null) {
            if (rewardItem.RewardType.Equals("heart")) {
                
            }
            else {
                var price = GetPrice(rewardItem.RewardType);
                if (_maxHp - price > 0) {
                    _maxHp -= price;
                    EnableAbility(rewardItem.RewardType);
                    rewardItem.Destroy();
                }
            }
        }
    }

    private void EnableAbility(string rewardItemRewardType) {
        var ability = _abilities.Find(x => x.Type.Equals(rewardItemRewardType));
        if (ability != null) {
            ability.Enable();
        }
    }

    private int GetPrice(string rewardType) {
        if (rewardType.Equals("bullet_time"))
            return _bulletTimeAbility.Price;
        if (rewardType.Equals("beam"))
            return _beamAbility.Price;
        if (rewardType.Equals("shield"))
            return _shieldAbility.Price;
        if (rewardType.Equals("explosion"))
            return _bombAbility.Price;

        return 0;
    }

    private void OnDrawGizmosSelected() {
        Gizmos.DrawLine(new Vector3(_bounds.xMin, _bounds.yMin), new Vector3(_bounds.xMax, _bounds.yMin));
        Gizmos.DrawLine(new Vector3(_bounds.xMin, _bounds.yMax), new Vector3(_bounds.xMax, _bounds.yMax));
        Gizmos.DrawLine(new Vector3(_bounds.xMin, _bounds.yMin), new Vector3(_bounds.xMin, _bounds.yMax));
        Gizmos.DrawLine(new Vector3(_bounds.xMax, _bounds.yMin), new Vector3(_bounds.xMax, _bounds.yMax));
    }

    public void RefillHp() {
        _hp = _maxHp;
    }

    public Ability.State GetAbilityState(string abilityType) {
        var ability = _abilities.Find(x => x.Type.Equals(abilityType));
        if (ability != null)
            return ability.CurrentState;

        return Ability.State.Disabled;
    }
}
