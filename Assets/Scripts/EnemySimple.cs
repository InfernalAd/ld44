﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySimple : Enemy {
    [SerializeField] private Vector2 _speed;
    protected override void Process (float deltaTime) {
        _weapons.ForEach(x => x.Shoot());
        var diff = _speed * deltaTime;
        transform.position += new Vector3(diff.x, diff.y);
        if (!LevelController.Instance.IsInBounds(transform.position)) {
            Destroy();
        }
    }
}
