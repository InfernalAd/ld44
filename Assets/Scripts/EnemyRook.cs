﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRook : Enemy {
    private enum State {
        FlyingOut = 0,
        WaitForShooting = 1,
        Shooting = 2,
        MovingLeft = 3,
        MovingRight = 4
    }

    public float TargetY = 1.5f;
    public float DispersionX = 0.5f;
    public Vector2 Speed;

    private float _targetX;
    private State _state = State.FlyingOut;


    protected override void Process(float deltaTime) {
        if (_state == State.FlyingOut) {
            if (transform.position.y > TargetY) {
                transform.position += new Vector3(0, Speed.y) * deltaTime;
                return;
            }
            else {
                _state = State.WaitForShooting;
            }
        }
        if (_state == State.WaitForShooting) {
            if (_weapons[0].CanShoot()) {
                _weapons[0].Shoot();
                _state = State.Shooting;
                return;
            }

        }
        if (_state == State.Shooting) {
            if (!_weapons[0].IsShooting()) {
                while (Mathf.Abs(transform.position.x - _targetX) < 0.4f) {
                    _targetX = Random.Range(-DispersionX, DispersionX);
                }
                _state = _targetX < transform.position.x ? State.MovingLeft : State.MovingRight;
            }
        }
        if (_state == State.MovingLeft) {
            if (transform.position.x <= _targetX) {
                _state = State.WaitForShooting;
            }
            else {
                transform.position += new Vector3(-Speed.x, 0) * deltaTime;
            }
        }
        if (_state == State.MovingRight) {
            if (transform.position.x >= _targetX) {
                _state = State.WaitForShooting;
            }
            else {
                transform.position += new Vector3(Speed.x, 0) * deltaTime;
            }
        }
    }
}
