﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HeartsPanel : MonoBehaviour {
	public List<Heart> Hearts = new List<Heart>();
	
	
	// Update is called once per frame
	void Update () {
		var maxHp = LevelController.Instance.Player.MaxHp;
		var hp = LevelController.Instance.Player.Hp;
		for (int i = 0; i != Hearts.Count; i++) {
			Hearts[i].SetActive(i < maxHp);
			Hearts[i].SetFilled(i < hp);
		}
	}
}
