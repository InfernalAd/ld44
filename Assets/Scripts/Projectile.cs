﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    private Vector2 _speed;
    private bool _active;
    public int Damage = 1;

    void Awake() {
        LevelController.Instance.OnProcess += Process;
    }
    
    public virtual void OnCollisionEnter() {
        Destroy();
    }

    public void InitAsPlayerProjectile(Vector2 speed) {
        gameObject.layer = LayerMask.NameToLayer("PlayerProjectile");
        _speed = speed;
        _active = true;
    }

    public void InitAsEnemyProjectile(Vector2 speed) {
        gameObject.layer = LayerMask.NameToLayer("EnemyProjectile");
        _speed = speed;
        _active = true;
    }

    public void InitAsBeam() {
        _speed = Vector2.zero;
        _active = true;
    }

    public void Destroy() {
        LevelController.Instance.OnProcess -= Process;
        GameObject.Destroy(gameObject);
    }

    protected virtual void Process(float deltaTime) {
        if (!_active) { return; }
        var speedVector = new Vector3(_speed.x, _speed.y, 0) * deltaTime;
        transform.position += speedVector;
        if (!LevelController.Instance.IsInBounds(transform.position))
            Destroy();
    }
}
