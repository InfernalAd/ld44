﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityButton : MonoBehaviour {

	public Sprite ActiveSprite;
	public Sprite InActiveSprite;
	public string AbilityType;
	public SpriteRenderer _renderer;

	void Update () {
		var state = LevelController.Instance.Player.GetAbilityState(AbilityType);
		switch (state) {
				case Ability.State.Disabled:
					_renderer.sprite = InActiveSprite;
					break;
				case Ability.State.WaitingForActication:
				
					_renderer.sprite = ActiveSprite;
					_renderer.color = Color.white;
					break;
			case Ability.State.Active:
			case Ability.State.Cooldown:
				_renderer.sprite = ActiveSprite;
				_renderer.color = Color.grey;
				break;
		}
	}
}
