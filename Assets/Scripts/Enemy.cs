﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    [SerializeField] protected List<Weapon> _weapons;
    [SerializeField] protected int _hp;
    public event Action  OnDestroyed = ()=> {};

    public void Awake() {
        LevelController.Instance.OnProcess += Process;
    }
    
    private void OnDestroy() {
        LevelController.Instance.OnProcess -= Process;
    }
    
    protected virtual void Process(float deltaTime) {
    }

    private void OnCollisionEnter2D(Collision2D coll) {
        var projectile = coll.gameObject.GetComponent<Projectile>();
        if (projectile == null) { return; }
        projectile.OnCollisionEnter();

        _hp-= projectile.Damage;
        if (_hp < 0) {
            LevelController.Instance.PlayExplosionFX(transform.position);
            Destroy();
        }
    }

    public void Destroy() {
        GameObject.Destroy(gameObject);
        OnDestroyed();
    }
}
