﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamWeapon : Weapon {
    public bool _shootingUp;
    private float _shootingTimer = 1f;

    private List<Projectile> _projectiles = new List<Projectile>();

    protected override void Process(float deltaTime) {
        if (_shootingTimer > 0) {
            _shootingTimer -= deltaTime;
        }
        else {
            base.Process(deltaTime);
        }
    }

    public override void Shoot() {
        if (_reloadLeft > 0) { return; }
        var positionDiff = Vector3.zero;
        for (int i = 0; i != 50; i++) {

            var proj = Instantiate(_projectilePrefab, transform);
            proj.InitAsBeam();
            proj.transform.position = transform.position + positionDiff;
            positionDiff += new Vector3(0, _shootingUp ? 0.06f : -0.06f);
        }
        _shootingTimer = 1.6f;
        _reloadLeft = _reload;
    }

    public override bool IsShooting() {
        return _shootingTimer > 0;
    }
}
