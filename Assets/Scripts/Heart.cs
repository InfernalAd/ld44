﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour {

	public GameObject HeartGO;
	public void SetFilled(bool active) {
		HeartGO.SetActive(active);
	}

	public void SetActive(bool active) {
		gameObject.SetActive(active);
	}
}
