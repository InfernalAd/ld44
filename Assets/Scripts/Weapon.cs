﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
    [SerializeField] protected Projectile _projectilePrefab;
    [SerializeField]
    protected float _reload;
    [SerializeField]
    protected bool _playerWeapon;
    [SerializeField]
    protected Vector2 _projectileSpeed;

    protected float _reloadLeft;

    void Awake() {
        LevelController.Instance.OnProcess += Process;
    }
    public virtual bool CanShoot() {
        return _reloadLeft <= 0;
    }

    public virtual bool IsShooting() {
        return false;
    }

    protected virtual void Process(float deltaTime) {
        _reloadLeft -= deltaTime;
    }

    private void OnDestroy() {
        LevelController.Instance.OnProcess -= Process;
    }

    public virtual void Shoot() {
        if (!CanShoot()) { return; }
        _reloadLeft = _reload;
        var projectile = GameObject.Instantiate(_projectilePrefab);
        projectile.transform.position = this.transform.position;
        if (_playerWeapon) {
            projectile.InitAsPlayerProjectile(_projectileSpeed);
        }
        else {
            projectile.InitAsEnemyProjectile(_projectileSpeed);
        }

    }
}
