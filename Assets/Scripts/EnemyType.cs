﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType  {
    Pawn = 0,
    Knight = 1,
    Bishop = 2,
    Rook = 3
}
