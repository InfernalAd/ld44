﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {
    [Serializable]
    public class Bounds {
        public float xMin;
        public float xMax;
        public float yMin;
        public float yMax;
    }
    [Serializable]
    public class EnemyPrefab {
        public EnemyType Type;
        public Enemy Prefab;
    }
    [Serializable]
    public class RewardPrefab {
        public string Type;
        public RewardItem Prefab;
    }
    public float MaxSpawnX = 1.6f;
    public float SpawnY = 3f;

    [SerializeField]
    private List<EnemyPrefab> _prefabs = new List<EnemyPrefab>();

    [SerializeField]
    private List<RewardPrefab> _rewardPrefabs = new List<RewardPrefab>();
    
    [SerializeField] private LevelController.Bounds _bounds;

    [SerializeField] private PlayerShip _playerShipPrefab;
    [SerializeField] private GameObject _explosionPrefab;
    [SerializeField] private GameObject _bombExplosionPrefab;
    private bool _bulletTimeActive;

    public static LevelController Instance { get; private set; }
    public event Action<float> OnProcess = (dt) => { };
    public event Action<float> OnProcessUnscaled = (dt) => { };
    
    public PlayerShip Player {
        get{
        return _player;
    }}
    
    private PlayerShip _player;
    private Level _level;
    private List<RewardItem> _rewardItems = new List<RewardItem>();
    private int _enemiesToSpawn;
    private int _aliveEnemies;
    private int _levelIndex = 1;

    private void Awake() {
        Instance = this;
        StartGame();
    }

    public void StartGame() {
        StartLevel(_levelIndex);
    }

    public void StartLevel(int levelIndex) {
        _level = Resources.Load<Level>("level_"+levelIndex);
        foreach (var item in _level.SpawnItems) {
            StartCoroutine(SpawnWithDelay(item.EnemyType, item.Delay, item.Position));
        }

        if (_player == null) {
            _player = Instantiate(_playerShipPrefab);
        }
        _player.RefillHp();
    }

    public void FinishLevel() {
        if (_level == null) return;
        float rewardX = -0.5f;
        foreach (var rewardType in _level.Rewards) {
            var prefabInfo = _rewardPrefabs.Find(x => x.Type.Equals(rewardType));
            if (prefabInfo != null) {
                var rewardItem = Instantiate(prefabInfo.Prefab, new Vector3(rewardX, SpawnY), Quaternion.identity);
                rewardItem.OnDestroyClicked += () => {
                    _rewardItems.ForEach(x => {
                        x.Destroy(false);
                        PlayExplosionFX(x.transform.position);
                    });
                    _rewardItems.Clear();
                
                    _levelIndex++;
                    StartLevel(_levelIndex);    
                };
                _rewardItems.Add(rewardItem);
                rewardX += 0.5f;
            }
        }
    }

    private IEnumerator SpawnWithDelay(EnemyType enemyType, float delay, float position) {
        _enemiesToSpawn++;
        yield return new WaitForSeconds(delay);
        var prefabInfo = _prefabs.Find(x => x.Type == enemyType);
        var newEnemy = Instantiate(prefabInfo.Prefab);
        newEnemy.transform.position = new Vector3(position * MaxSpawnX, SpawnY);
        _aliveEnemies++;
        newEnemy.OnDestroyed += () => { _aliveEnemies--; };
        _enemiesToSpawn--;
    }

    public void PlayExplosionFX(Vector3 position) {
        Instantiate(_explosionPrefab, position,Quaternion.identity);
    }
    
    public void CreateBombExplosion(Vector3 position) {
        Instantiate(_bombExplosionPrefab, position,Quaternion.identity);
    }

    private void FixedUpdate() {
        var deltaTime = _bulletTimeActive ? Time.fixedDeltaTime * 0.5f : Time.fixedDeltaTime;
        OnProcess(deltaTime);
        OnProcessUnscaled(Time.fixedDeltaTime);
        if (_enemiesToSpawn == 0 && _aliveEnemies == 0 && _rewardItems.Count == 0) {
            if (_levelIndex <5)
                FinishLevel();
            else {
                FinishGame();
            }
        }
    }

    private void FinishGame() {
    }

    public void SetBulletTimeActive(bool active) {
        _bulletTimeActive = active;
    }

    public bool IsInBounds(Vector3 position) {
        return position.x > _bounds.xMin && position.x < _bounds.xMax && position.y > _bounds.yMin &&
               position.y < _bounds.yMax;
    }
    
    private void OnDrawGizmosSelected() {
        Gizmos.DrawLine(new Vector3(_bounds.xMin, _bounds.yMin), new Vector3(_bounds.xMax, _bounds.yMin));
        Gizmos.DrawLine(new Vector3(_bounds.xMin, _bounds.yMax), new Vector3(_bounds.xMax, _bounds.yMax));
        Gizmos.DrawLine(new Vector3(_bounds.xMin, _bounds.yMin), new Vector3(_bounds.xMin, _bounds.yMax));
        Gizmos.DrawLine(new Vector3(_bounds.xMax, _bounds.yMin), new Vector3(_bounds.xMax, _bounds.yMax));
    }
}
