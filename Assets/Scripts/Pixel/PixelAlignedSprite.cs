﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PixelAlignedSprite : MonoBehaviour {
    void Update () {
        Snap();
    }

    private void Snap() {
        var x  = Mathf.Floor(transform.position.x * 100) / 100f;
        var y = Mathf.Floor(transform.position.y * 100) / 100f;
        var z = Mathf.Floor(transform.position.z * 100) / 100f;

        transform.position = new Vector3(x, y, z);
    }
}
