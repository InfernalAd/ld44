﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardItem : MonoBehaviour {

	public string RewardType;
	private bool _destroyed;

	public event Action OnDestroyClicked = () => { };

	public void Destroy(bool sendEvent = true) {
		if (_destroyed) return;;
		_destroyed = true;
		if (sendEvent)
		OnDestroyClicked();
		GameObject.Destroy(gameObject);
	}

	private void Awake() {
		LevelController.Instance.OnProcess += Process;
	}
	
	private void OnDestroy() {
		LevelController.Instance.OnProcess -= Process;
	}

	private void Process(float dt) {
		if (transform.position.y > 0) {
			transform.position += Vector3.down * dt;
		}
	}
}
