﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "", menuName = "LD/Level")]
public class Level : ScriptableObject {
    [Serializable]
    public class SpawnItem {
        public EnemyType EnemyType;
        public float Delay;
        [Range(-1, 1)]
        public float Position;
    }

    public int WaveNumber;
    public List<SpawnItem> SpawnItems;
    public List<string> Rewards;
}
